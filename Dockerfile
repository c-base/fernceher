FROM docker.io/library/alpine
RUN apk --no-cache add firefox ttf-dejavu ttf-liberation ttf-freefont ttf-opensans ttf-font-awesome
RUN apk add xvfb x11vnc terminus-font
RUN apk add xterm fluxbox
RUN apk add ffmpeg
RUN apk add chromium
COPY entrypoint.sh /entrypoint.sh
EXPOSE 5900
ENTRYPOINT [ "/entrypoint.sh" ]
