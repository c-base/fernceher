#!/bin/sh

set -eu

DISPLAY=:0
export DISPLAY

Xvfb ${DISPLAY} -screen ${DISPLAY} 720x480x24 &

#sleep 3

#x11vnc -display ${DISPLAY} -listen 0.0.0.0 -forever &

#sleep 3

startfluxbox &

self_ip=$(hostname -i)

#ffmpeg -y -f x11grab -video_size 1920x1080 -r 12 -i $DISPLAY -codec:v libx264 -f flv "rtmp://mediamtx:8554/matemon"
#ffmpeg -y -f x11grab -video_size 1920x1080 -r 12 -i $DISPLAY -codec:v libx264 -profile:v baseline -pix_fmt yuv420p -f rtsp -rtsp_transport tcp "rtsp://mediamtx:8554/matemon" &
ffmpeg -y -f x11grab -video_size 720x480 -r 12 -i $DISPLAY -codec:v libx264 -profile:v baseline -pix_fmt yuv420p -f rtsp -rtsp_transport tcp "rtsp://10.0.1.102:8554/matemontest" &
#ffmpeg -y -f x11grab -video_size 1920x1080 -r 12 -i $DISPLAY -codec:v libx264 -f mpegts "udp://${self_ip}:5900"
#sleep 3

# C0D1 = 49361
echo "Hello from ctreamer"

echo "$@"
exec "$@"
